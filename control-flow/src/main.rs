fn count_take() {
    let mut counter = -1;
    let result = loop {
        counter += 1;

        if counter == 10 {
            break counter + 2;
        }
    };

    println!("The result is {}", result)
}

fn count_down(x: u32) {
    let mut number = x;
    while number != 0 {
        println!("{}!", number);
        number -= 1;
    }
}
fn count_down_five() {
    let numbers = [1, 2, 3, 4, 5];
    for numbers in numbers.iter() {
        println!("{}!", numbers);
    }
}

fn count_from_five() {
    for number in(1..6).rev() {
        println!("{}!", number);
    }
}

fn main() {
    count_take();
    count_down(10);
    count_down_five();
    count_from_five();
}
